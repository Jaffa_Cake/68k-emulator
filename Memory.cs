using J68K.Instructions;

namespace J68K
{
    public class Memory
    {
        public const Byte BLANK = 0x00;

        public static Byte[] ram = new Byte[0x1FFFF + 1];
        public static Byte[] rom = new Byte[0x1FFFF + 1];

        public static UInt32 readUInt32(UInt32 address)
        {
            UInt32 roof = ((UInt32)readByte(address) << 24);
            UInt32 ceiling = ((UInt32)readByte(address + 1) << 16);
            UInt32 walls = ((UInt32)readByte(address + 2) << 8);
            UInt32 floor = (UInt32)readByte(address + 3);

            return roof | ceiling | walls | floor;
        }

        public static UInt16 readUInt16(UInt32 address)
        {
            UInt16 hi = (UInt16)((UInt16)(readByte(address)) << 8);
            Byte lo = readByte(address + 1);

            return (UInt16)(hi | lo);
        }

        public static Byte readByte(UInt32 address)
        {
            if (address >= 0x00000000 && address <= 0x0001FFFF)
            {
                return ram[address];
            }
            else if (address >= 0x00400000 && address <= 0x0041FFFF)
            {
                return rom[address - 0x00400000];
            }

            return BLANK;
        }

        public static void writeData(UInt32 address, ESize size, UInt32 data)
        {
            switch (size)
            {
                case ESize.BYTE: writeByte(address, (Byte)data); break;
                case ESize.WORD: writeUInt16(address, (UInt16)data); break;
                case ESize.LONG: writeUInt32(address, data); break;
            }
        }

        public static void writeByte(UInt32 address, Byte data)
        {
            if (address >= 0x00000000 && address <= 0x0001FFFF)
            {
                ram[address] = data;
            }
        }

        public static void writeUInt16(UInt32 address, UInt16 data)
        {
            writeByte(address, (Byte)(data >> 8));
            writeByte(address + 1, (Byte)(data));
        }

        public static void writeUInt32(UInt32 address, UInt32 data)
        {
            writeByte(address, (Byte)(data >> 24));
            writeByte(address + 1, (Byte)(data >> 16));
            writeByte(address + 2, (Byte)(data >> 8));
            writeByte(address + 3, (Byte)(data));
        }
    }
}
