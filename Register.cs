namespace J68K
{
    public abstract class ARegister
    {
        public UInt32 value;
    }

    public class Register8 : ARegister
    {
        public Register8() { }
        public Register8(Byte value) { this.value = value; }

        public static implicit operator Byte(Register8 i) => (Byte)i.value;
        public static implicit operator Register8(Byte i) => new Register8(i);
    }

    public class Register16 : ARegister
    {
        public Register16() { }
        public Register16(UInt16 value) { this.value = value; }

        public static implicit operator UInt16(Register16 i) => (UInt16)i.value;
        public static implicit operator Register16(UInt16 i) => new Register16(i);
    }

    public class Register32 : ARegister
    {
        public Register32() { }
        public Register32(UInt32 value) { this.value = value; }

        public static implicit operator UInt32(Register32 i) => (UInt32)i.value;
        public static implicit operator Register32(UInt32 i) => new Register32(i);
    }
}