using static J68K.Program;
using static J68K.InstructionEvaluator;

namespace J68K.Instructions
{
    public class InstructionCLR : AInstruction
    {
        public ESize size;
        public EAddressingMode addressingMode;
        public ERegister register;
        public UInt16[] extension;

        public InstructionCLR(UInt16[] data) : base("CLR", "Clear an Operand")
        {
            size = evaluateSize(data[0], 6);
            evaluateAddressingModeAndRegister(data[0], 0, out addressingMode, out register, 0b101111_111000, false);

            if (addressingMode == EAddressingMode.INVALID)
            {
                throw new Exception("Loaded invalid operation.");
            }

            extension = getExtension(addressingMode, size, data, 1);
            length = 1 + extension.Length;
        }

        public override Boolean execute()
        {
            negative = false;
            zero = true;
            overflow = false;
            carry = false;

            setEffectiveAddressData(addressingMode, extension, register, size, 0);

            return true;
        }

        public override int getCycles()
        {
            switch (addressingMode)
            {
                case EAddressingMode.ADDRESS_REGISTER_DIRECT:
                case EAddressingMode.DATA_REGISTER_DIRECT:
                    {
                        if (size == ESize.LONG)
                            return 6;
                        else
                            return 4;
                    }

                default:
                    {
                        if (size == ESize.LONG)
                            return 12;
                        else
                            return 8;
                    }
            }
        }

        public override String ToString()
        {
            return mnemonic + " - " + name + " - " + addressingMode.ToString();
        }
    }
}