using static J68K.Program;

namespace J68K.Instructions
{
    public class InstructionRTS : AInstruction
    {
        public InstructionRTS() : base("RTS", "Return from Subroutine") { }

        public override Boolean execute()
        {
            pc = Memory.readUInt32(sp++) - 1;
            return false;
        }

        public override int getLength()
        {
            return 1;
        }

        public override String ToString()
        {
            return mnemonic + " - " + name;
        }
    }
}