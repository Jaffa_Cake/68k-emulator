namespace J68K.Instructions
{
    public abstract class AInstruction
    {
        public readonly String mnemonic, name;
        protected int length;

        public AInstruction(String mnemonic, String name)
        {
            this.mnemonic = mnemonic;
            this.name = name;
        }

        public virtual int getCycles()
        {
            return 4;
        }

        public virtual int getLength()
        {
            return length;
        }

        public abstract Boolean execute();

        public abstract override String ToString();
    }
}