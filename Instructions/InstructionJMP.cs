using static J68K.Program;
using static J68K.InstructionEvaluator;

namespace J68K.Instructions
{
    public class InstructionJMP : AInstruction
    {
        public EAddressingMode addressingMode;
        public ERegister register;
        public UInt16[] extension;

        public InstructionJMP(UInt16[] data) : base("JMP", "Jump")
        {
            evaluateAddressingModeAndRegister(data[0], 0, out addressingMode, out register, 0b001001_111110, false);

            if (addressingMode == EAddressingMode.INVALID)
            {
                throw new Exception("Loaded invalid operation.");
            }

            extension = getExtension(addressingMode, ESize.INVALID, data, 1);
            length = 1 + extension.Length;
        }

        public override Boolean execute()
        {
            UInt32 newAddress = getEffectiveAddressAddress(addressingMode, extension, register, ESize.INVALID);
            pc.value = newAddress;
            return false;
        }

        public override int getCycles()
        {
            switch (addressingMode)
            {
                case EAddressingMode.ADDRESS_REGISTER_INDIRECT: return 8;
                case EAddressingMode.ABSOLUTE_SHORT: return 10;
                case EAddressingMode.ABSOLUTE_LONG: return 12;
                default: return 4;
            }
        }

        public override String ToString()
        {
            return mnemonic + " - " + name + " - " + addressingMode.ToString();
        }
    }

}