using static J68K.Program;
using static J68K.InstructionEvaluator;

namespace J68K.Instructions
{
    public class InstructionMOVE : AInstruction
    {
        public EAddressingMode destinationMode;
        public ERegister destinationRegister;
        public EAddressingMode sourceMode;
        public ERegister sourceRegister;
        public ESize size;

        public UInt16[] destinationExtension;
        public UInt16[] sourceExtension;


        public InstructionMOVE(UInt16[] data) : base("MOVE", "Move Data from Source to Destination")
        {
            size = evaluateSize(data[0], 12, 0b01, 0b11, 0b10);
            evaluateAddressingModeAndRegister(data[0], 6, out destinationMode, out destinationRegister, 0b101111_111000, true);
            evaluateAddressingModeAndRegister(data[0], 0, out sourceMode, out sourceRegister, 0b111111_111111, false);

            if (size == ESize.INVALID || destinationMode == EAddressingMode.INVALID || sourceMode == EAddressingMode.INVALID)
            {
                throw new Exception("Loaded invalid operation. Illegal size or addressing mode.");
            }
            if (sourceMode == EAddressingMode.ADDRESS_REGISTER_DIRECT && size == ESize.BYTE)
            {
                throw new Exception("Loaded invalid operation. Illegal to move Byte into address register.");
            }

            destinationExtension = getExtension(destinationMode, size, data, 1);
            sourceExtension = getExtension(sourceMode, size, data, 1 + destinationExtension.Length);

            length = 1 + destinationExtension.Length + sourceExtension.Length;
        }

        public override Boolean execute()
        {
            UInt32 data = getEffectiveAddressData(sourceMode, sourceExtension, sourceRegister, size);

            negative = readBit(data, 31);
            zero = data == 0x00000000;
            overflow = false;
            carry = false;
            // eXtend not affected

            setEffectiveAddressData(destinationMode, destinationExtension, destinationRegister, size, data);

            return true;
        }

        public override string ToString()
        {
            return mnemonic + " - " + name + " - " + sourceMode.ToString() + " to " + destinationMode.ToString();
        }
    }
}