namespace J68K.Instructions
{
    public class InstructionNOP : AInstruction
    {
        public InstructionNOP() : base("NOP", "No Operation") { length = 1; }
        public override Boolean execute() { return true; }
        public override int getCycles() { return 4; }
        public override String ToString() { return mnemonic + " - " + name; }
    }
}