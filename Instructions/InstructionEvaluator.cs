using J68K.Instructions;

using static J68K.Program;

namespace J68K
{
    public class InstructionEvaluator
    {
        public const Byte MASK_EA = 0b00111111;
        public const Byte MASK_EA_MODE = 0b00111000;
        public const Byte MASK_EA_REGISTER = 0b00000111;
        public const Byte EA_MODE_SPECIAL = 0b00111000;

        public static Dictionary<Byte, ESize> sizes = new Dictionary<Byte, ESize>()
        {
            {0b00, ESize.BYTE},
            {0b01, ESize.WORD},
            {0b10, ESize.LONG}
        };

        public static Dictionary<Byte, EAddressingMode> addressingModes = new Dictionary<Byte, EAddressingMode>()
        {
            {0b000000, EAddressingMode.DATA_REGISTER_DIRECT},
            {0b001000, EAddressingMode.ADDRESS_REGISTER_DIRECT},
            {0b010000, EAddressingMode.ADDRESS_REGISTER_INDIRECT},
            {0b011000, EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_POSTINCREMENT},
            {0b100000, EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_PREDECREMENT},
            {0b101000, EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_DISPLACEMENT},
            {0b110000, EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_INDEX},
            {0b111000, EAddressingMode.ABSOLUTE_SHORT},
            {0b111001, EAddressingMode.ABSOLUTE_LONG},
            {0b111010, EAddressingMode.PROGRAM_COUNTER_WITH_DISPLACEMENT},
            {0b111011, EAddressingMode.PROGRAM_COUNTER_WITH_INDEX},
            {0b111100, EAddressingMode.IMMEDIATE}
        };

        public static Dictionary<Byte, ERegister> dataRegisters = new Dictionary<Byte, ERegister>()
        {
            {0b000, ERegister.DATA_0},
            {0b001, ERegister.DATA_1},
            {0b010, ERegister.DATA_2},
            {0b011, ERegister.DATA_3},
            {0b100, ERegister.DATA_4},
            {0b101, ERegister.DATA_5},
            {0b110, ERegister.DATA_6},
            {0b111, ERegister.DATA_7}
        };

        public static Dictionary<Byte, ERegister> addressRegisters = new Dictionary<Byte, ERegister>()
        {
            {0b000, ERegister.ADDRESS_0},
            {0b001, ERegister.ADDRESS_1},
            {0b010, ERegister.ADDRESS_2},
            {0b011, ERegister.ADDRESS_3},
            {0b100, ERegister.ADDRESS_4},
            {0b101, ERegister.ADDRESS_5},
            {0b110, ERegister.ADDRESS_6},
            {0b111, ERegister.ADDRESS_7}
        };

        public static Boolean determineIfTargetsAddressRegister(EAddressingMode addressingMode)
        {
            switch (addressingMode)
            {
                case EAddressingMode.DATA_REGISTER_DIRECT: return false;
                default: return true;
            }
        }

        public static UInt16[] getExtension(EAddressingMode mode, ESize size, UInt16[] data, int offset)
        {
            switch (mode)
            {
                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_DISPLACEMENT:
                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_INDEX:
                case EAddressingMode.ABSOLUTE_SHORT:
                case EAddressingMode.PROGRAM_COUNTER_WITH_DISPLACEMENT:
                case EAddressingMode.PROGRAM_COUNTER_WITH_INDEX:
                    return new UInt16[] { data[0 + offset] };

                case EAddressingMode.ABSOLUTE_LONG:
                    return new UInt16[] { data[0 + offset], data[1 + offset] };

                case EAddressingMode.IMMEDIATE:
                    {
                        if (size == ESize.LONG)
                            return new UInt16[] { data[0 + offset], data[1 + offset] };
                        else
                            return new UInt16[] { data[0 + offset] };
                    }

                default:
                    return new UInt16[] { };
            }
        }

        public static UInt32 pickData(UInt32 data, int location, UInt32 mask)
        {
            return (data & (mask << location)) >> location;
        }

        public static ESize evaluateSize(UInt32 data, int location)
        {
            ESize evaluation = ESize.INVALID;
            sizes.TryGetValue((Byte)(pickData(data, location, 0b11)), out evaluation);
            return evaluation;
        }

        public static ESize evaluateSize(UInt32 data, int location, Byte size8, Byte size16, Byte size32)
        {
            Byte pickedData = (Byte)pickData(data, location, 0b11);
            if (pickedData == size8) return ESize.BYTE;
            else if (pickedData == size16) return ESize.WORD;
            else if (pickedData == size32) return ESize.LONG;
            else return ESize.INVALID;
        }

        public static EAddressingMode evaluateAddressingMode(UInt32 data, int location, Boolean registerFirst)
        {
            EAddressingMode evaluation = EAddressingMode.DATA_REGISTER_DIRECT;
            if (registerFirst)
            {
                addressingModes.TryGetValue((Byte)((Byte)(pickData(data, location, 0b111) << 3) | (Byte)(pickData(data, location + 3, 0b111))), out evaluation);
            }
            else
            {
                addressingModes.TryGetValue((Byte)(pickData(data, location, 0b111111)), out evaluation);
            }
            return evaluation;
        }

        public static ERegister evaluateRegister(UInt32 data, int location, Boolean addressRegister)
        {
            ERegister evaluation = addressRegister ? ERegister.ADDRESS_0 : ERegister.DATA_0;
            Dictionary<Byte, ERegister> specificRegisterDictionary = addressRegister ? addressRegisters : dataRegisters;
            specificRegisterDictionary.TryGetValue((Byte)(pickData(data, location, 0b111)), out evaluation);
            return evaluation;
        }


        public static void evaluateAddressingModeAndRegister(
            UInt32 data,
            int location,
            out EAddressingMode addressingMode,
            out ERegister register,
            UInt16 modeMask,
            Boolean registerFirst)
        {
            // Addressing Mode is SPECIAL
            if (pickData(data, location + (registerFirst ? 0 : 3), 0b111) == 0b111)
            {
                addressingMode = evaluateAddressingMode(pickData(data, location, 0b111111), 0, registerFirst);
                register = ERegister.DATA_0;
            }

            // Addressing Mode is NORMAL
            else
            {
                addressingMode = evaluateAddressingMode(pickData(data, location, (Byte)(registerFirst ? 0b000111 : 0b111000)), 0, registerFirst);
                register = evaluateRegister(data, location + (registerFirst ? 3 : 0), determineIfTargetsAddressRegister(addressingMode));
            }

            if (((UInt16)addressingMode & modeMask) == 0)
            {
                addressingMode = EAddressingMode.INVALID;
            }
        }


        public static Int32 evaluateIndexOffset(UInt16 data)
        {
            ERegister indexRegister = evaluateRegister(data, 12, readBit(data, 15));
            Boolean indexSize = readBit(data, 11);
            SByte displacement = (SByte)pickData(data, 0, 0b11111111);

            Int32 index = 0x00000000;
            if (indexSize)
            {
                index = (Int32)getRegister(indexRegister).value;
            }
            else
            {
                index = (Int32)signExtend((UInt16)getRegister(indexRegister).value);
            }

            return index + displacement;
        }


        public static AInstruction evaluate(UInt16 data)
        {
            return evaluate(new UInt16[] { data });
        }

        public static AInstruction evaluate(UInt16[] data) // REMEMBER TO GIVE UP TO 5(?) WORDS OF DATA
        {
            UInt16 head = data[0];

            switch (head)
            {
                case 0x4E71: return new InstructionNOP();
                case 0x4E75: return new InstructionRTS();
            }

            switch (head & 0b1111111111000000)
            {
                case 0x4EC0: return new InstructionJMP(data);
            }

            switch (head & 0b1111111100000000)
            {
                case 0x4200: return new InstructionCLR(data);
            }

            switch (head & 0b11000000_00000000)
            {
                case 0x0000: return new InstructionMOVE(data);
            }

            return null;
        }
    }
}