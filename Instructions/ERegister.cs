namespace J68K.Instructions
{
    public enum ERegister
    {
        ADDRESS_0,
        ADDRESS_1,
        ADDRESS_2,
        ADDRESS_3,
        ADDRESS_4,
        ADDRESS_5,
        ADDRESS_6,
        ADDRESS_7,

        DATA_0,
        DATA_1,
        DATA_2,
        DATA_3,
        DATA_4,
        DATA_5,
        DATA_6,
        DATA_7,

        STACK_POINTER,
        USER_STACK_POINTER,
        SUPERVISOR_STACK_POINTER,

        PROGRAM_COUNTER,

        STATUS_REGISTER,
        CONDITION_CODE_REGISTER
    }
}