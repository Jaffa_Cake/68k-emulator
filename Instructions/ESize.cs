namespace J68K.Instructions
{
    public enum ESize
    {
        BYTE,
        WORD,
        LONG,

        INVALID
    }
}