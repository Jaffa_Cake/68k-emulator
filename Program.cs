﻿using J68K.Instructions;

using static J68K.InstructionEvaluator;

namespace J68K
{
    public class Program
    {
        public static Register32[] d = new Register32[8];
        public static Register32[] a = new Register32[7];
        public static Register32 usp;
        public static Register32 ssp;
        public static Register32 sp
        {
            get { return supervisor ? ssp : usp; }
            set { if (supervisor) ssp = value; else usp = value; }
        }
        public static Register32 pc;
        public static Register8 ccr
        {
            get { return (Byte)(sr & 0x00FF); }
            set { sr = (UInt16)((sr & 0xFF00) | (value & 0x00FF)); }
        }
        private static UInt16 _sr;
        public static Register16 sr
        {
            get { return (UInt16)(_sr & 0b1010011100011111); }
            set { _sr = (UInt16)(value & 0b1010011100011111); }
        }

        public static Boolean trace
        {
            get { return readBit(sr, 15); }
            set { sr = setBit(sr, 15, value); }
        }
        public static Boolean supervisor
        {
            get { return readBit(sr, 13); }
            set { sr = setBit(sr, 13, value); }
        }
        public static Byte interruptMask
        {
            get { return (Byte)((sr & 0x0700) >> 8); }
            set
            {
                sr = setBit(sr, 8, (value & 0x01) != 0);
                sr = setBit(sr, 9, (value & 0x02) != 0);
                sr = setBit(sr, 10, (value & 0x04) != 0);
            }
        }
        public static Boolean extend
        {
            get { return readBit(sr, 4); }
            set { sr = setBit(sr, 4, value); }
        }
        public static Boolean negative
        {
            get { return readBit(sr, 3); }
            set { sr = setBit(sr, 3, value); }
        }
        public static Boolean zero
        {
            get { return readBit(sr, 2); }
            set { sr = setBit(sr, 2, value); }
        }
        public static Boolean overflow
        {
            get { return readBit(sr, 1); }
            set { sr = setBit(sr, 1, value); }
        }
        public static Boolean carry
        {
            get { return readBit(sr, 0); }
            set { sr = setBit(sr, 0, value); }
        }

        public static void Main(String[] args)
        {
            reset();

            Memory.writeUInt16(0x0000, 0b0100111001110001);
            Memory.writeUInt16(0x0002, 0b0100111001110001);
            Memory.writeUInt16(0x0004, 0b00_10_111000_111100);
            Memory.writeUInt32(0x0006, 0x11111111);
            Memory.writeUInt16(0x000A, 0b00_11_110000_111100);
            Memory.writeUInt16(0x000C, 0x2222);
            Memory.writeUInt16(0x000E, 0b00_01_101000_111100);
            Memory.writeUInt16(0x0010, 0x0033);
            Memory.writeUInt16(0x0012, 0b0100111001110001);
            Memory.writeUInt16(0x0014, 0b0100111001110001);
            Memory.writeUInt16(0x0016, 0b01000010_00_000_111);
            Memory.writeUInt16(0x0018, 0b0100111001110001);

            ThreadStart runStart = new ThreadStart(run);
            Thread thread = new Thread(runStart);
            thread.Start();

            return;
        }

        public static void run()
        {
            AInstruction instruction;

            for (int i = 0; i < 20; i++)
            {
                UInt16[] workingData = new UInt16[5];
                for (int x = 0; x < workingData.Length; x++)
                {
                    workingData[x] = Memory.readUInt16((UInt32)(pc + (x * 2)));
                }

                dumpRegisters();
                instruction = InstructionEvaluator.evaluate(workingData);

                if (instruction == null)
                {
                    Console.WriteLine(" > {0:X8}:{1:X4} > NULL INSTRUCTION", pc.value, workingData[0]);
                    break;
                }

                Console.WriteLine(" > {0:X8}:{1:X4} > {2}", pc.value, workingData[0], instruction.ToString());
                Boolean advancePC = instruction.execute();

                Thread.Sleep(instruction.getCycles() * 20);

                if (advancePC)
                {
                    for (int l = 0; l < instruction.getLength(); l++)
                    {
                        pc.value++;
                        pc.value++;
                    }
                }
            }
        }

        public static void reset()
        {
            for (int n = 0; n < d.Length; n++)
            {
                d[n] = 0x00000000;
            }
            for (int n = 0; n < a.Length; n++)
            {
                a[n] = 0x00000000;
            }

            usp = 0x00000000;
            ssp = 0x00000000;
            pc = 0x00000000;
            sr = 0x0000;

            supervisor = true;
        }

        public static void dumpRegisters()
        {
            Console.WriteLine("PC:{0:X8} SR:{1:X4}     US:{2:X8} SS:{3:X8}", pc.value, sr.value, usp.value, ssp.value);
            Console.WriteLine("D0:{0:X8} D1:{1:X8} D2:{2:X8} D3:{3:X8}", d[0].value, d[1].value, d[2].value, d[3].value);
            Console.WriteLine("D4:{0:X8} D5:{1:X8} D6:{2:X8} D7:{3:X8}", d[4].value, d[5].value, d[6].value, d[7].value);
            Console.WriteLine("A0:{0:X8} A1:{1:X8} A2:{2:X8} A3:{3:X8}", a[0].value, a[1].value, a[2].value, a[3].value);
            Console.WriteLine("A4:{0:X8} A5:{1:X8} A6:{2:X8} A7:{3:X8}", a[4].value, a[5].value, a[6].value, sp.value);
            Console.WriteLine("-----------------------------------------------");
        }

        public static UInt32 getEffectiveAddressAddress(EAddressingMode mode, UInt16[] extension, ERegister register, ESize size)
        {
            switch (mode)
            {
                case EAddressingMode.ADDRESS_REGISTER_INDIRECT:
                    {
                        return getRegister(register).value;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_POSTINCREMENT:
                    {
                        return getRegister(register).value++;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_PREDECREMENT:
                    {
                        return --getRegister(register).value;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_DISPLACEMENT:
                    {
                        Int16 displacement = (Int16)extension[0];
                        return (UInt32)(getRegister(register).value + displacement);
                    }

                case EAddressingMode.PROGRAM_COUNTER_WITH_INDEX:
                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_INDEX:
                    {
                        UInt32 address;

                        if (mode == EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_INDEX)
                        {
                            address = (UInt32)(getRegister(register).value + evaluateIndexOffset(extension[0]));
                        }
                        else
                        {
                            address = (UInt32)(getRegister(ERegister.PROGRAM_COUNTER).value + evaluateIndexOffset(extension[0]));
                        }

                        return address;
                    }

                case EAddressingMode.ABSOLUTE_SHORT:
                    {
                        return signExtend(extension[0]);
                    }

                case EAddressingMode.ABSOLUTE_LONG:
                    {
                        return combineWords(extension[0], extension[1]);
                    }

                case EAddressingMode.PROGRAM_COUNTER_WITH_DISPLACEMENT:
                    {
                        return getRegister(ERegister.PROGRAM_COUNTER).value + signExtend(extension[0]);
                    }

                case EAddressingMode.INVALID:
                case EAddressingMode.IMMEDIATE:
                case EAddressingMode.DATA_REGISTER_DIRECT:
                case EAddressingMode.ADDRESS_REGISTER_DIRECT:
                default:
                    {
                        throw new Exception("Tried to evaluate an INVALID effective address (is there a problem further up the chain?)");
                    }
            }
        }

        public static UInt32 getEffectiveAddressData(EAddressingMode mode, UInt16[] extension, ERegister register, ESize size)
        {
            switch (mode)
            {
                case EAddressingMode.DATA_REGISTER_DIRECT:
                case EAddressingMode.ADDRESS_REGISTER_DIRECT:
                    {
                        return getRegister(register).value;
                    }

                case EAddressingMode.IMMEDIATE:
                    {
                        if (size == ESize.BYTE) return (Byte)extension[0];
                        else if (size == ESize.WORD) return extension[0];
                        else if (size == ESize.LONG) return (UInt32)(extension[0] << 16) | (UInt32)(extension[1]);
                        else throw new Exception("Invalid size passed when trying to resolve effective address data!");
                    }

                // TODO Add invalid check here
                default:
                    {
                        return Memory.readUInt32(getEffectiveAddressAddress(mode, extension, register, size));
                    }
            }
        }

        public static void setEffectiveAddressData(EAddressingMode mode, UInt16[] extension, ERegister register, ESize size, UInt32 data)
        {
            switch (mode)
            {
                case EAddressingMode.DATA_REGISTER_DIRECT:
                case EAddressingMode.ADDRESS_REGISTER_DIRECT:
                    {
                        setRegister(register, size, data);
                        break;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT:
                    {
                        Memory.writeData(getRegister(register).value, size, data);
                        break;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_POSTINCREMENT:
                    {
                        Memory.writeData(getRegister(register).value++, size, data);
                        break;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_PREDECREMENT:
                    {
                        Memory.writeData(--getRegister(register).value, size, data);
                        break;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_DISPLACEMENT:
                    {
                        Memory.writeData(getRegister(register).value + signExtend(extension[0]), size, data);
                        break;
                    }

                case EAddressingMode.ADDRESS_REGISTER_INDIRECT_WITH_INDEX:
                    {
                        Memory.writeData((UInt32)(getRegister(register).value + evaluateIndexOffset(extension[0])), size, data);
                        break;
                    }

                case EAddressingMode.ABSOLUTE_SHORT:
                    {
                        Memory.writeData(signExtend(extension[0]), size, data);
                        break;
                    }

                case EAddressingMode.ABSOLUTE_LONG:
                    {
                        Memory.writeData(combineWords(extension[0], extension[1]), size, data);
                        break;
                    }

                case EAddressingMode.PROGRAM_COUNTER_WITH_DISPLACEMENT:
                    {
                        Memory.writeData(getRegister(ERegister.PROGRAM_COUNTER).value + signExtend(extension[0]), size, data);
                        break;
                    }

                case EAddressingMode.PROGRAM_COUNTER_WITH_INDEX:
                    {
                        Memory.writeData((UInt32)(getRegister(ERegister.PROGRAM_COUNTER).value + evaluateIndexOffset(extension[0])), size, data);
                        break;
                    }

                case EAddressingMode.IMMEDIATE:
                case EAddressingMode.INVALID:
                default:
                    {
                        throw new Exception("Tried writing data with INVALID addressing mode. Note that IMMEDIATE is also not allowed.");
                    }

            }
        }

        public static ARegister getRegister(ERegister register)
        {
            switch (register)
            {
                case ERegister.DATA_0: return d[0];
                case ERegister.DATA_1: return d[1];
                case ERegister.DATA_2: return d[2];
                case ERegister.DATA_3: return d[3];
                case ERegister.DATA_4: return d[4];
                case ERegister.DATA_5: return d[5];
                case ERegister.DATA_6: return d[6];
                case ERegister.DATA_7: return d[7];
                case ERegister.ADDRESS_0: return a[0];
                case ERegister.ADDRESS_1: return a[1];
                case ERegister.ADDRESS_2: return a[2];
                case ERegister.ADDRESS_3: return a[3];
                case ERegister.ADDRESS_4: return a[4];
                case ERegister.ADDRESS_5: return a[5];
                case ERegister.ADDRESS_6: return a[6];
                case ERegister.ADDRESS_7: return sp;
                case ERegister.CONDITION_CODE_REGISTER: return ccr;
                case ERegister.PROGRAM_COUNTER: return pc;
                case ERegister.STACK_POINTER: return sp;
                case ERegister.STATUS_REGISTER: return sr;
                case ERegister.SUPERVISOR_STACK_POINTER: return ssp;
                case ERegister.USER_STACK_POINTER: return usp;
                default: return new Register32(0x00000000);
            }
        }

        public static void setRegister(ERegister register, ESize dataSize, UInt32 data)
        {
            switch (dataSize)
            {
                case ESize.BYTE:
                    {
                        getRegister(register).value &= 0xFFFFFF00;
                        getRegister(register).value |= (Byte)data;
                        break;
                    }

                case ESize.WORD:
                    {
                        getRegister(register).value &= 0xFFFF0000;
                        getRegister(register).value |= (UInt16)data;
                        break;
                    }

                case ESize.LONG:
                    {
                        getRegister(register).value = data;
                        break;
                    }
            }
        }

        public static UInt32 signExtend(UInt16 number)
        {
            if (readBit(number, 15))
            {
                return (UInt32)(0xFFFF0000 | number);
            }
            else
            {
                return (UInt32)number;
            }
        }

        public static UInt32 combineWords(UInt16 high, UInt16 low)
        {
            return (UInt32)(high << 16) | low;
        }

        public static Boolean readBit(UInt32 data, int index)
        {
            return (data & (UInt32)(0x01 << index)) != 0;
        }

        public static Byte setBit(Byte data, int index, Boolean state)
        {
            if (state) data |= (Byte)(0x01 << index); else data &= (Byte)~(0x01 << index);
            return data;
        }

        public static UInt16 setBit(UInt16 data, int index, Boolean state)
        {
            if (state) data |= (UInt16)(0x01 << index); else data &= (UInt16)~(0x01 << index);
            return data;
        }

        public static UInt32 setBit(UInt32 data, int index, Boolean state)
        {
            if (state) data |= (UInt32)(0x01 << index); else data &= (UInt32)~(0x01 << index);
            return data;
        }
    }
}
